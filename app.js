const http = require('http');
const crypto = require('crypto');
const dotenv = require('dotenv');

dotenv.config();

const port = process.env.PORT;
const htmlUrl = process.env.HTML_URL
const jsonUrl = process.env.JSON_URL
const uuidUrl = process.env.UUID_URL
const statusUrl = process.env.STATUS_URL
const delayUrl = process.env.DELAY_URL

const server = http.createServer(function (req, res) {
    // console.log(req.url);
    const url = req.url;
    if (url === htmlUrl) {
        res.write(`<!DOCTYPE html>
        <html>
          <head>
          </head>
          <body>
              <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
              <p> - Martin Fowler</p>
        
          </body>
        </html>`);
        res.end();
    } else if (url === jsonUrl) {
        res.write(`
        {
            "slideshow": {
              "author": "Yours Truly",
              "date": "date of publication",
              "slides": [
                {
                  "title": "Wake up to WonderWidgets!",
                  "type": "all"
                },
                {
                  "items": [
                    "Why <em>WonderWidgets</em> are great",
                    "Who <em>buys</em> WonderWidgets"
                  ],
                  "title": "Overview",
                  "type": "all"
                }
              ],
              "title": "Sample Slide Show"
            }
          }
        `);
        res.end();
    } else if (url === uuidUrl) {
        const newUuid = crypto.randomUUID();
        // console.log(newUuid);
        res.write(`{
            "uuid": ${newUuid}
          }`);
        res.end();
    } else if (url.includes(statusUrl)) {
        const urlArr = url.split("/");
        const status = urlArr[urlArr.length - 1];
        res.statusCode = status;
        res.write(`
        <html>
            <body>
                <h1> Status Code : ${status} </h1>
            </body>
        </html>
        `);
        res.end();

    } else if (url.includes(delayUrl)) {
        // console.log(url);
        const urlArr = url.split("/");
        const time = +urlArr[urlArr.length - 1];

        setTimeout(() => {
            res.statusCode = 200;
            res.write(
            `<html>
                <body>
                    <h1> Success </h1>
                </body>
            </html>
            `);
            res.end();
        }, time * 1000);

    }
    else{
        res.write(`
        <html>
            <body>
                <h1> This is a Homepage. </h1>
            </body>
        </html>
        `);
        res.end();
    }
});


server.listen(port, function () {
    console.log("Server is running !");
});